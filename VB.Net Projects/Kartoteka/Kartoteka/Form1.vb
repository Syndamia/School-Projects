﻿Imports System.ComponentModel ' Required for BindingList
Imports System.IO             ' Required for File
Imports Newtonsoft.Json       ' Required for JsonConvert

Public Class Form1
    ' Only administrators can write to "C:\", so instead we save the file to the desktop of the current user
    ReadOnly filePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\Desktop\data.json"

    ' Normal lists don't give out a "signal" when a value is added, which we need so cbNames updates when we update people
    ReadOnly people As BindingList(Of Person) = New BindingList(Of Person)

    Private Sub initDataSource(sender As Object, e As EventArgs) Handles MyBase.Load
        cbNames.DataSource = people
    End Sub

    Private Sub resetInputTab(sender As Object, e As EventArgs) Handles btnClear.Click, MyBase.Load
        For Each control As Control In {txtFirstName, txtMiddleName, txtLastName, txtPhoneNumber, txtEmail, lblStatus, dtBirth}
            control.ResetText()
        Next
    End Sub

    Private Sub btnInput_Click(sender As Object, e As EventArgs) Handles btnInput.Click
        people.Add(New Person(txtFirstName.Text, txtMiddleName.Text, txtLastName.Text, txtPhoneNumber.Text, txtEmail.Text, dtBirth.Value))
        lblStatus.Text = "Въведено!"
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        ' Json Serialization is easier, more universal and more flexible than having a custom serialization
        File.WriteAllText(filePath, JsonConvert.SerializeObject(people))
        lblStatus.Text = "Данните са запазени във файла успешно!"
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        Dim deserializedPeople = JsonConvert.DeserializeObject(Of List(Of Person))(File.ReadAllText(filePath))
        people.Clear()
        deserializedPeople.ForEach(Sub(p) people.Add(p))

        lblStatus.Text = "Данните от файла са заредени успешно!"
    End Sub

    Private Sub btnGetReport_Click(sender As Object, e As EventArgs) Handles btnGetReport.Click
        Dim selectedPerson As Person = people(cbNames.SelectedIndex)
        lblPersonData.Text = $"Информация за:{vbCrLf}{vbCrLf}" &
                             $"Имена: {selectedPerson}{vbCrLf}" &
                             $"Телефон: {selectedPerson.PhoneNumber}{vbCrLf}" &
                             $"E-mail: {selectedPerson.Email}{vbCrLf}" &
                             $"Рожденна дата: {selectedPerson.BirthDay}{vbCrLf}"
    End Sub
End Class