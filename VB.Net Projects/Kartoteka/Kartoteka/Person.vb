﻿Public Class Person
    Public FirstName, MiddleName, LastName, PhoneNumber, Email As String
    Public BirthDay As Date

    Public Sub New(firstName As String, middleName As String, lastName As String, phoneNumber As String, email As String, birthDay As Date)
        Me.FirstName = firstName
        Me.MiddleName = middleName
        Me.LastName = lastName
        Me.PhoneNumber = phoneNumber
        Me.Email = email
        Me.BirthDay = birthDay
    End Sub

    Public Overrides Function ToString() As String
        Return $"{Me.FirstName} {Me.MiddleName} {Me.LastName}"
    End Function
End Class