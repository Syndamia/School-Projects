﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbSize = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.chkKolbas = New System.Windows.Forms.CheckBox()
        Me.chkCheese = New System.Windows.Forms.CheckBox()
        Me.chkOlive = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'cbSize
        '
        Me.cbSize.FormattingEnabled = True
        Me.cbSize.Items.AddRange(New Object() {"Малка", "Средна", "Голяма"})
        Me.cbSize.Location = New System.Drawing.Point(210, 29)
        Me.cbSize.Name = "cbSize"
        Me.cbSize.Size = New System.Drawing.Size(107, 21)
        Me.cbSize.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.Location = New System.Drawing.Point(37, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(92, 25)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Добавки"
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(212, 136)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(105, 39)
        Me.btnCalculate.TabIndex = 5
        Me.btnCalculate.Text = "Изчисли"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'chkKolbas
        '
        Me.chkKolbas.AutoSize = True
        Me.chkKolbas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.chkKolbas.Location = New System.Drawing.Point(34, 69)
        Me.chkKolbas.Name = "chkKolbas"
        Me.chkKolbas.Size = New System.Drawing.Size(97, 29)
        Me.chkKolbas.TabIndex = 6
        Me.chkKolbas.Text = "Колбас"
        Me.chkKolbas.UseVisualStyleBackColor = True
        '
        'chkCheese
        '
        Me.chkCheese.AutoSize = True
        Me.chkCheese.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.chkCheese.Location = New System.Drawing.Point(34, 104)
        Me.chkCheese.Name = "chkCheese"
        Me.chkCheese.Size = New System.Drawing.Size(99, 29)
        Me.chkCheese.TabIndex = 7
        Me.chkCheese.Text = "Сирене"
        Me.chkCheese.UseVisualStyleBackColor = True
        '
        'chkOlive
        '
        Me.chkOlive.AutoSize = True
        Me.chkOlive.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.chkOlive.Location = New System.Drawing.Point(34, 139)
        Me.chkOlive.Name = "chkOlive"
        Me.chkOlive.Size = New System.Drawing.Size(112, 29)
        Me.chkOlive.TabIndex = 8
        Me.chkOlive.Text = "Маслини"
        Me.chkOlive.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 204)
        Me.Controls.Add(Me.chkOlive)
        Me.Controls.Add(Me.chkCheese)
        Me.Controls.Add(Me.chkKolbas)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbSize)
        Me.Name = "Form1"
        Me.Text = "Поръчка на пица"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbSize As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents chkKolbas As System.Windows.Forms.CheckBox
    Friend WithEvents chkCheese As System.Windows.Forms.CheckBox
    Friend WithEvents chkOlive As System.Windows.Forms.CheckBox

End Class
