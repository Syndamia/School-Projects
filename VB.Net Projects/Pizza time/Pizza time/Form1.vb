﻿Public Class Form1
    Dim sum As Double  
    Private Sub cbSize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSize.SelectedIndexChanged
        Select Case cbSize.Text
            Case "Малка"
                sum = 6
            Case "Средна"
                sum = 8
            Case "Голяма"
                sum = 10
        End Select
    End Sub   

    Private Sub chkKolbas_CheckedChanged(sender As Object, e As EventArgs) Handles chkKolbas.CheckedChanged
        sum += 1
    End Sub

    Private Sub chkCheese_CheckedChanged(sender As Object, e As EventArgs) Handles chkCheese.CheckedChanged
        sum += 1
    End Sub

    Private Sub chkOlive_CheckedChanged(sender As Object, e As EventArgs) Handles chkOlive.CheckedChanged
        sum += 1
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        MsgBox("Сумата Ви е " & sum, 0, "Сума")

        sum = 0
        chkKolbas.Checked = False
        chkCheese.Checked = False
        chkOlive.Checked = False
        cbSize.Text = vbNullString
    End Sub
End Class
