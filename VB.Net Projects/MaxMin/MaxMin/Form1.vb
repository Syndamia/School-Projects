﻿Public Class Form1
    Const numberCount = 10, minimumPossible = 1, maxiumPossible = 100

    Dim numbers(numberCount - 1) As Integer
    Dim rng As Random = New Random

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click, MyBase.Load
        ClearAnswer()

        RefreshNumbers()
        lblNumbers.Text = String.Join(" ", numbers)
    End Sub

    Private Sub btnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click
        UpdateCorrectness(lblMin, txtMin.Text.Trim() = numbers.Min()) ' Somehow it handles this comparison
        UpdateCorrectness(lblMax, txtMax.Text.Trim() = numbers.Max()) ' Somehow it handles this comparison

        btnNew.Enabled = True
        btnCheck.Enabled = False
        txtMax.Enabled = False
        txtMin.Enabled = False

        lblNumbers.Text = String.Join(" ", numbers.OrderBy(Function(x) x))
    End Sub

    Private Sub txtMinOrtxtMax_TextChanged(sender As Object, e As EventArgs) Handles txtMin.TextChanged, txtMax.TextChanged
        ' If both text inputs aren't empty, enable the "Check" button
        btnCheck.Enabled = Not String.IsNullOrEmpty(txtMin.Text) And Not String.IsNullOrEmpty(txtMax.Text)
    End Sub

    Private Sub RefreshNumbers()
        For index = 0 To (numberCount - 1) Step 1
            numbers(index) = rng.Next(minimumPossible, maxiumPossible + 1) ' Exclusive upper bound
            index -= numbers.Count(Function(x) x = numbers(index)) - 1 ' numbers.Count(...) is always between 1 and 2
        Next
    End Sub

    Private Sub ClearAnswer()
        lblMin.Text = String.Empty
        txtMin.Text = String.Empty
        txtMin.Enabled = True

        lblMax.Text = String.Empty
        txtMax.Text = String.Empty
        txtMax.Enabled = True

        btnNew.Enabled = False
    End Sub

    Private Sub UpdateCorrectness(control As Control, isCorrect As Boolean)
        If isCorrect Then
            control.Text = "OK"
            control.ForeColor = Color.Green
        Else
            control.Text = "X"
            control.ForeColor = Color.Red
        End If
    End Sub
End Class
