﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.lblSize = New System.Windows.Forms.Label()
        Me.lblCount = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt9x13 = New System.Windows.Forms.TextBox()
        Me.txt10x15 = New System.Windows.Forms.TextBox()
        Me.txt13x18 = New System.Windows.Forms.TextBox()
        Me.txt15x21 = New System.Windows.Forms.TextBox()
        Me.btnSum = New System.Windows.Forms.Button()
        Me.lblSum = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblHeader
        '
        Me.lblHeader.AutoSize = True
        Me.lblHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblHeader.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblHeader.Location = New System.Drawing.Point(3, 9)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(299, 32)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Формуляр за поръчка за отпечатване " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "на снимки"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblSize
        '
        Me.lblSize.AutoSize = True
        Me.lblSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblSize.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblSize.Location = New System.Drawing.Point(36, 65)
        Me.lblSize.Name = "lblSize"
        Me.lblSize.Size = New System.Drawing.Size(65, 20)
        Me.lblSize.TabIndex = 1
        Me.lblSize.Text = "Размер"
        '
        'lblCount
        '
        Me.lblCount.AutoSize = True
        Me.lblCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblCount.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCount.Location = New System.Drawing.Point(198, 65)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(47, 20)
        Me.lblCount.TabIndex = 2
        Me.lblCount.Text = "Брой"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(37, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "9x13 - 0,19 лв"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(34, 134)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "10х15 - 0,24 лв"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(34, 164)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "13х18 - 0,35 лв"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(34, 194)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "15х21 - 0,55 лв"
        '
        'txt9x13
        '
        Me.txt9x13.Location = New System.Drawing.Point(169, 101)
        Me.txt9x13.Name = "txt9x13"
        Me.txt9x13.Size = New System.Drawing.Size(100, 20)
        Me.txt9x13.TabIndex = 7
        '
        'txt10x15
        '
        Me.txt10x15.Location = New System.Drawing.Point(169, 131)
        Me.txt10x15.Name = "txt10x15"
        Me.txt10x15.Size = New System.Drawing.Size(100, 20)
        Me.txt10x15.TabIndex = 8
        '
        'txt13x18
        '
        Me.txt13x18.Location = New System.Drawing.Point(169, 161)
        Me.txt13x18.Name = "txt13x18"
        Me.txt13x18.Size = New System.Drawing.Size(100, 20)
        Me.txt13x18.TabIndex = 9
        '
        'txt15x21
        '
        Me.txt15x21.Location = New System.Drawing.Point(169, 191)
        Me.txt15x21.Name = "txt15x21"
        Me.txt15x21.Size = New System.Drawing.Size(100, 20)
        Me.txt15x21.TabIndex = 10
        '
        'btnSum
        '
        Me.btnSum.Location = New System.Drawing.Point(29, 220)
        Me.btnSum.Name = "btnSum"
        Me.btnSum.Size = New System.Drawing.Size(92, 37)
        Me.btnSum.TabIndex = 11
        Me.btnSum.Text = "Сума"
        Me.btnSum.UseVisualStyleBackColor = True
        '
        'lblSum
        '
        Me.lblSum.AutoSize = True
        Me.lblSum.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblSum.ForeColor = System.Drawing.Color.Firebrick
        Me.lblSum.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblSum.Location = New System.Drawing.Point(199, 230)
        Me.lblSum.Name = "lblSum"
        Me.lblSum.Size = New System.Drawing.Size(46, 16)
        Me.lblSum.TabIndex = 12
        Me.lblSum.Text = "Сума"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(304, 269)
        Me.Controls.Add(Me.lblSum)
        Me.Controls.Add(Me.btnSum)
        Me.Controls.Add(Me.txt15x21)
        Me.Controls.Add(Me.txt13x18)
        Me.Controls.Add(Me.txt10x15)
        Me.Controls.Add(Me.txt9x13)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCount)
        Me.Controls.Add(Me.lblSize)
        Me.Controls.Add(Me.lblHeader)
        Me.Name = "Form1"
        Me.Text = "Печат на снимки"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents lblSize As System.Windows.Forms.Label
    Friend WithEvents lblCount As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt9x13 As System.Windows.Forms.TextBox
    Friend WithEvents txt10x15 As System.Windows.Forms.TextBox
    Friend WithEvents txt13x18 As System.Windows.Forms.TextBox
    Friend WithEvents txt15x21 As System.Windows.Forms.TextBox
    Friend WithEvents btnSum As System.Windows.Forms.Button
    Friend WithEvents lblSum As System.Windows.Forms.Label

End Class
