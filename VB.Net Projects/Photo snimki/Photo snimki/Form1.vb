﻿Public Class Form1
    Dim Sum As Double
    Dim Br As Integer
    Dim P9, P10, P13, P15 As Double
    Private Sub btnSum_Click(sender As Object, e As EventArgs) Handles btnSum.Click
        If txt9x13.Text <> "" Then
            P9 = 0.19 * Val(txt9x13.Text)
        Else
            P9 = 0
        End If

        If txt10x15.Text <> "" Then
            P10 = 0.24 * Val(txt10x15.Text)
        Else
            P10 = 0
        End If

        If txt13x18.Text <> "" Then
            P13 = 0.35 * Val(txt13x18.Text)
        Else
            P13 = 0
        End If

        If txt15x21.Text <> "" Then
            P15 = 0.55 * Val(txt15x21.Text)
        Else
            P15 = 0
        End If

        Sum = P9 + P10 + P13 + P15
        Br = Val(txt9x13.Text) + Val(txt10x15.Text) + Val(txt13x18.Text) + Val(txt15x21.Text)

        Select Case Br
            Case 1 To 40
                lblSum.Text = FormatCurrency(Sum, 2)
            Case 41 To 80
                lblSum.Text = 0.95 * FormatCurrency(Sum, 2)
            Case 81 To 110
                lblSum.Text = 0.9 * FormatCurrency(Sum, 2)
            Case 111 To 150
                lblSum.Text = 0.85 * FormatCurrency(Sum, 2)
            Case Else
                lblSum.Text = 0.8 * FormatCurrency(Sum, 2)
        End Select
    End Sub
End Class
