﻿Partial Class Form1 ' PurchaseTab

    Private Sub DefinePurchaseTabDataSources() Handles MyBase.Load
        cbReport.DataSource = Books
        cbGenre.DataSource = Book.Genres
    End Sub

    Private Sub PurchaseSelectedBook() Handles btnPurchase.Click
        If Books(cbPurchase.SelectedIndex).Amount = 0 Then
            MsgBox("Няма налични издания на тази книга!", MsgBoxStyle.Critical, "Грешка!")
            Return
        ElseIf Not IsNumeric(txtAmountToBuy.Text) Then
            MsgBox("Трябва да въведете брой на закупени издания!", MsgBoxStyle.Critical, "Грешка!")
            Return
        ElseIf txtAmountToBuy.Text > Books(cbPurchase.SelectedIndex).Amount Then
            MsgBox($"Има само {Books(cbPurchase.SelectedIndex).Amount} брой налични издания!", MsgBoxStyle.Critical, "Грешка!")
            Return
        End If

        Dim purchasedBook = cbPurchase.SelectedItem
        Purchases.Add(New Purchase(purchasedBook.Title, purchasedBook.Price, txtAmountToBuy.Text))
        purchasedBook.Amount -= txtAmountToBuy.Text
    End Sub

    Private Sub ClearPurchases() Handles btnClearPurchases.Click, btnLoad.Click
        Purchases.Clear()
        txtAmountToBuy.ResetText()
    End Sub

    Private Sub UpdateItemPriceLabel() Handles cbPurchase.SelectedIndexChanged, btnLoad.Click, MyBase.Load
        If cbPurchase.SelectedIndex > -1 Then
            lblPrice.Text = FormatCurrency(Books(cbPurchase.SelectedIndex).Price, 2)
        Else
            lblPrice.Text = FormatCurrency(0, 2)
        End If
    End Sub

    Private Sub UpdateSumLabel() Handles btnPurchase.Click, btnClearPurchases.Click, btnLoad.Click, MyBase.Load
        lblSum.Text = FormatCurrency(Purchases.Sum(Function(p) p.ItemPrice * p.Amount), 2)
    End Sub
End Class