﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tbInputTab = New System.Windows.Forms.TabPage()
        Me.cbGenre = New System.Windows.Forms.ComboBox()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtPublisher = New System.Windows.Forms.TextBox()
        Me.txtAuthor = New System.Windows.Forms.TextBox()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbReport = New System.Windows.Forms.ComboBox()
        Me.lblTotalCount = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCreateNew = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnReport = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.tbPurchaseTab = New System.Windows.Forms.TabPage()
        Me.btnClearPurchases = New System.Windows.Forms.Button()
        Me.lblSum = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lbChosenBooks = New System.Windows.Forms.ListBox()
        Me.btnPurchase = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtAmountToBuy = New System.Windows.Forms.TextBox()
        Me.cbPurchase = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.tbInputTab.SuspendLayout()
        Me.tbPurchaseTab.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tbInputTab)
        Me.TabControl1.Controls.Add(Me.tbPurchaseTab)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(460, 337)
        Me.TabControl1.TabIndex = 0
        '
        'tbInputTab
        '
        Me.tbInputTab.Controls.Add(Me.cbGenre)
        Me.tbInputTab.Controls.Add(Me.txtAmount)
        Me.tbInputTab.Controls.Add(Me.txtPrice)
        Me.tbInputTab.Controls.Add(Me.txtPublisher)
        Me.tbInputTab.Controls.Add(Me.txtAuthor)
        Me.tbInputTab.Controls.Add(Me.txtTitle)
        Me.tbInputTab.Controls.Add(Me.Label8)
        Me.tbInputTab.Controls.Add(Me.Label7)
        Me.tbInputTab.Controls.Add(Me.Label6)
        Me.tbInputTab.Controls.Add(Me.Label5)
        Me.tbInputTab.Controls.Add(Me.Label4)
        Me.tbInputTab.Controls.Add(Me.Label3)
        Me.tbInputTab.Controls.Add(Me.cbReport)
        Me.tbInputTab.Controls.Add(Me.lblTotalCount)
        Me.tbInputTab.Controls.Add(Me.Label1)
        Me.tbInputTab.Controls.Add(Me.btnCreateNew)
        Me.tbInputTab.Controls.Add(Me.btnAdd)
        Me.tbInputTab.Controls.Add(Me.btnReport)
        Me.tbInputTab.Controls.Add(Me.btnSave)
        Me.tbInputTab.Controls.Add(Me.btnLoad)
        Me.tbInputTab.Location = New System.Drawing.Point(4, 24)
        Me.tbInputTab.Name = "tbInputTab"
        Me.tbInputTab.Padding = New System.Windows.Forms.Padding(3)
        Me.tbInputTab.Size = New System.Drawing.Size(452, 309)
        Me.tbInputTab.TabIndex = 0
        Me.tbInputTab.Text = "Въвеждане"
        Me.tbInputTab.UseVisualStyleBackColor = True
        '
        'cbGenre
        '
        Me.cbGenre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbGenre.FormattingEnabled = True
        Me.cbGenre.Location = New System.Drawing.Point(110, 189)
        Me.cbGenre.Name = "cbGenre"
        Me.cbGenre.Size = New System.Drawing.Size(331, 23)
        Me.cbGenre.TabIndex = 21
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(246, 263)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(97, 23)
        Me.txtAmount.TabIndex = 20
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(58, 263)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(120, 23)
        Me.txtPrice.TabIndex = 19
        '
        'txtPublisher
        '
        Me.txtPublisher.Location = New System.Drawing.Point(110, 218)
        Me.txtPublisher.Name = "txtPublisher"
        Me.txtPublisher.Size = New System.Drawing.Size(331, 23)
        Me.txtPublisher.TabIndex = 17
        '
        'txtAuthor
        '
        Me.txtAuthor.Location = New System.Drawing.Point(110, 162)
        Me.txtAuthor.Name = "txtAuthor"
        Me.txtAuthor.Size = New System.Drawing.Size(331, 23)
        Me.txtAuthor.TabIndex = 16
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(110, 133)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(331, 23)
        Me.txtTitle.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(199, 266)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 15)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Брой:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 266)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(38, 15)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Цена:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(11, 221)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 15)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Издателство:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(11, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 15)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Жанр:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 165)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 15)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Автор:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 15)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Заглавие:"
        '
        'cbReport
        '
        Me.cbReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbReport.FormattingEnabled = True
        Me.cbReport.Location = New System.Drawing.Point(110, 42)
        Me.cbReport.Name = "cbReport"
        Me.cbReport.Size = New System.Drawing.Size(331, 23)
        Me.cbReport.TabIndex = 7
        '
        'lblTotalCount
        '
        Me.lblTotalCount.AutoSize = True
        Me.lblTotalCount.ForeColor = System.Drawing.Color.Red
        Me.lblTotalCount.Location = New System.Drawing.Point(248, 11)
        Me.lblTotalCount.Name = "lblTotalCount"
        Me.lblTotalCount.Size = New System.Drawing.Size(27, 15)
        Me.lblTotalCount.TabIndex = 6
        Me.lblTotalCount.Text = "ERR"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(116, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 15)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Общ брой заглавия:"
        '
        'btnCreateNew
        '
        Me.btnCreateNew.Location = New System.Drawing.Point(11, 89)
        Me.btnCreateNew.Name = "btnCreateNew"
        Me.btnCreateNew.Size = New System.Drawing.Size(113, 23)
        Me.btnCreateNew.TabIndex = 4
        Me.btnCreateNew.Text = "Ново заглавие"
        Me.btnCreateNew.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(364, 263)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(77, 23)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "Добави"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnReport
        '
        Me.btnReport.Location = New System.Drawing.Point(11, 42)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(77, 23)
        Me.btnReport.TabIndex = 2
        Me.btnReport.Text = "Справка"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(364, 7)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(77, 23)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Запази"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(11, 7)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(77, 23)
        Me.btnLoad.TabIndex = 0
        Me.btnLoad.Text = "Зареди"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'tbPurchaseTab
        '
        Me.tbPurchaseTab.Controls.Add(Me.btnClearPurchases)
        Me.tbPurchaseTab.Controls.Add(Me.lblSum)
        Me.tbPurchaseTab.Controls.Add(Me.Label12)
        Me.tbPurchaseTab.Controls.Add(Me.lbChosenBooks)
        Me.tbPurchaseTab.Controls.Add(Me.btnPurchase)
        Me.tbPurchaseTab.Controls.Add(Me.Label11)
        Me.tbPurchaseTab.Controls.Add(Me.lblPrice)
        Me.tbPurchaseTab.Controls.Add(Me.Label9)
        Me.tbPurchaseTab.Controls.Add(Me.txtAmountToBuy)
        Me.tbPurchaseTab.Controls.Add(Me.cbPurchase)
        Me.tbPurchaseTab.Controls.Add(Me.Label2)
        Me.tbPurchaseTab.Location = New System.Drawing.Point(4, 24)
        Me.tbPurchaseTab.Name = "tbPurchaseTab"
        Me.tbPurchaseTab.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPurchaseTab.Size = New System.Drawing.Size(452, 309)
        Me.tbPurchaseTab.TabIndex = 1
        Me.tbPurchaseTab.Text = "Продажба"
        Me.tbPurchaseTab.UseVisualStyleBackColor = True
        '
        'btnClearPurchases
        '
        Me.btnClearPurchases.Location = New System.Drawing.Point(353, 268)
        Me.btnClearPurchases.Name = "btnClearPurchases"
        Me.btnClearPurchases.Size = New System.Drawing.Size(77, 26)
        Me.btnClearPurchases.TabIndex = 10
        Me.btnClearPurchases.Text = "Изчисти"
        Me.btnClearPurchases.UseVisualStyleBackColor = True
        '
        'lblSum
        '
        Me.lblSum.AutoSize = True
        Me.lblSum.ForeColor = System.Drawing.Color.Red
        Me.lblSum.Location = New System.Drawing.Point(58, 274)
        Me.lblSum.Name = "lblSum"
        Me.lblSum.Size = New System.Drawing.Size(27, 15)
        Me.lblSum.TabIndex = 9
        Me.lblSum.Text = "ERR"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(13, 274)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(39, 15)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Сума:"
        '
        'lbChosenBooks
        '
        Me.lbChosenBooks.FormattingEnabled = True
        Me.lbChosenBooks.ItemHeight = 15
        Me.lbChosenBooks.Location = New System.Drawing.Point(13, 107)
        Me.lbChosenBooks.Name = "lbChosenBooks"
        Me.lbChosenBooks.Size = New System.Drawing.Size(417, 139)
        Me.lbChosenBooks.TabIndex = 7
        '
        'btnPurchase
        '
        Me.btnPurchase.Location = New System.Drawing.Point(353, 60)
        Me.btnPurchase.Name = "btnPurchase"
        Me.btnPurchase.Size = New System.Drawing.Size(77, 23)
        Me.btnPurchase.TabIndex = 6
        Me.btnPurchase.Text = "Добави"
        Me.btnPurchase.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(150, 63)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(38, 15)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Брой:"
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.ForeColor = System.Drawing.Color.Blue
        Me.lblPrice.Location = New System.Drawing.Point(57, 63)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(27, 15)
        Me.lblPrice.TabIndex = 4
        Me.lblPrice.Text = "ERR"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 63)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(38, 15)
        Me.Label9.TabIndex = 3
        Me.Label9.Text = "Цена:"
        '
        'txtAmountToBuy
        '
        Me.txtAmountToBuy.Location = New System.Drawing.Point(203, 60)
        Me.txtAmountToBuy.Name = "txtAmountToBuy"
        Me.txtAmountToBuy.Size = New System.Drawing.Size(62, 23)
        Me.txtAmountToBuy.TabIndex = 2
        '
        'cbPurchase
        '
        Me.cbPurchase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPurchase.FormattingEnabled = True
        Me.cbPurchase.Location = New System.Drawing.Point(85, 13)
        Me.cbPurchase.Name = "cbPurchase"
        Me.cbPurchase.Size = New System.Drawing.Size(345, 23)
        Me.cbPurchase.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Книга:"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 361)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Form1"
        Me.Text = "Книжарница"
        Me.TabControl1.ResumeLayout(False)
        Me.tbInputTab.ResumeLayout(False)
        Me.tbInputTab.PerformLayout()
        Me.tbPurchaseTab.ResumeLayout(False)
        Me.tbPurchaseTab.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tbInputTab As TabPage
    Friend WithEvents cbGenre As ComboBox
    Friend WithEvents txtAmount As TextBox
    Friend WithEvents txtPrice As TextBox
    Friend WithEvents txtPublisher As TextBox
    Friend WithEvents txtAuthor As TextBox
    Friend WithEvents txtTitle As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cbReport As ComboBox
    Friend WithEvents lblTotalCount As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnCreateNew As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnReport As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents btnLoad As Button
    Friend WithEvents tbPurchaseTab As TabPage
    Friend WithEvents btnClearPurchases As Button
    Friend WithEvents lblSum As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents lbChosenBooks As ListBox
    Friend WithEvents btnPurchase As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents lblPrice As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtAmountToBuy As TextBox
    Friend WithEvents cbPurchase As ComboBox
    Friend WithEvents Label2 As Label
End Class
