﻿Imports System.ComponentModel

Public Class Book
    Public Shared ReadOnly Genres As BindingList(Of String) = New BindingList(Of String) From {
        "Хуманитанитарна литература",
        "Художествена литература",
        "Психология",
        "Компютри",
        "Чужди езици",
        "Техника",
        "Медицина",
        "Икономика",
        "Справочна литература",
        "Право",
        "Хоби",
        "Психология",
        "Друг"
    }

    Public Title, Publisher, Author As String
    Public Price As Single
    Public GenreIndex, Amount As Integer

    Public Sub New(title As String, publisher As String, author As String, genreIndex As Integer, price As Single, amount As Integer)
        Me.Title = title
        Me.Publisher = publisher
        Me.Author = author
        Me.GenreIndex = genreIndex
        Me.Price = price
        Me.Amount = amount
    End Sub

    Public Overrides Function ToString() As String
        Return Me.Title
    End Function
End Class
