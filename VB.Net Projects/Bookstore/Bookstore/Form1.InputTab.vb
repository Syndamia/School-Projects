﻿Imports System.IO
Imports Newtonsoft.Json

Partial Class Form1 ' InputTab

    Private Sub DefineInputTabDataSources() Handles MyBase.Load
        cbPurchase.DataSource = Books
        lbChosenBooks.DataSource = Purchases
    End Sub

    Private Sub AddBook() Handles btnAdd.Click
        If Not IsNumeric(txtPrice.Text) Or Not IsNumeric(txtAmount.Text) Then
            MsgBox("Цена и брой могат да бъдат само числа!", MsgBoxStyle.Critical, "Грешка!")
            Return
        End If

        Books.Add(New Book(txtTitle.Text, txtPublisher.Text, txtAuthor.Text, cbGenre.SelectedIndex, txtPrice.Text, txtAmount.Text))
        MsgBox("Книгата е добавена!", MsgBoxStyle.OkOnly, "Успех!")
    End Sub

    Private Sub SaveToFile() Handles btnSave.Click
        File.WriteAllText(filePath, JsonConvert.SerializeObject(Books))
        MsgBox("Данните са запазени!", MsgBoxStyle.OkOnly, "Успех!")
    End Sub

    Private Sub LoadFromFile() Handles btnLoad.Click
        Dim deserializedBooks = JsonConvert.DeserializeObject(Of List(Of Book))(File.ReadAllText(filePath))
        Books.Clear()
        deserializedBooks.ForEach(Sub(b) Books.Add(b))
        MsgBox("Данните са заредени!", MsgBoxStyle.OkOnly, "Успех!")
    End Sub

    Private Sub ShowReportBook() Handles btnReport.Click
        If cbReport.SelectedItem Is Nothing Then
            Return
        End If

        Dim selectedBook As Book = cbReport.SelectedItem
        txtTitle.Text = selectedBook.Title
        txtAuthor.Text = selectedBook.Author
        txtPublisher.Text = selectedBook.Publisher
        txtPrice.Text = selectedBook.Price
        txtAmount.Text = selectedBook.Amount
        cbGenre.SelectedIndex = selectedBook.GenreIndex
    End Sub

    Private Sub ClearInputs() Handles btnCreateNew.Click, MyBase.Load
        For Each control As Control In {txtTitle, txtAuthor, txtAmount, txtPrice, txtPublisher}
            control.ResetText()
        Next
    End Sub

    Private Sub UpdateTotalCountLabel() Handles btnAdd.Click, btnLoad.Click, MyBase.Load
        lblTotalCount.Text = Books.Count()
    End Sub
End Class