﻿Public Class Purchase
    Public ItemTitle As String
    Public ItemPrice As Single
    Public Amount As Integer

    Public Sub New(itemTitle As String, itemPrice As Single, amount As Integer)
        Me.ItemTitle = itemTitle
        Me.ItemPrice = itemPrice
        Me.Amount = amount
    End Sub

    Public Overrides Function ToString() As String
        Return $"{Me.Amount} {Me.ItemTitle}"
    End Function
End Class