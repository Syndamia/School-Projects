﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formCarRental
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFormHeading = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblSurname = New System.Windows.Forms.Label()
        Me.lblAdress = New System.Windows.Forms.Label()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtSurname = New System.Windows.Forms.TextBox()
        Me.txtAdress = New System.Windows.Forms.TextBox()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.lblCarModel = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.lblBonus = New System.Windows.Forms.Label()
        Me.chkChair = New System.Windows.Forms.CheckBox()
        Me.chkSki = New System.Windows.Forms.CheckBox()
        Me.chkChain = New System.Windows.Forms.CheckBox()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.gbPayment = New System.Windows.Forms.GroupBox()
        Me.optCredit = New System.Windows.Forms.RadioButton()
        Me.optBank = New System.Windows.Forms.RadioButton()
        Me.optCash = New System.Windows.Forms.RadioButton()
        Me.lblDateReturn = New System.Windows.Forms.Label()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.gbPayment.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblFormHeading
        '
        Me.lblFormHeading.AutoSize = True
        Me.lblFormHeading.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.lblFormHeading.Font = New System.Drawing.Font("Verdana", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormHeading.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblFormHeading.Location = New System.Drawing.Point(53, 9)
        Me.lblFormHeading.Name = "lblFormHeading"
        Me.lblFormHeading.Size = New System.Drawing.Size(487, 42)
        Me.lblFormHeading.TabIndex = 0
        Me.lblFormHeading.Text = "Резервация на автомобил"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblName.Location = New System.Drawing.Point(88, 77)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(42, 18)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Име "
        '
        'lblSurname
        '
        Me.lblSurname.AutoSize = True
        Me.lblSurname.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblSurname.Location = New System.Drawing.Point(73, 106)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(73, 18)
        Me.lblSurname.TabIndex = 0
        Me.lblSurname.Text = "Фамилия"
        '
        'lblAdress
        '
        Me.lblAdress.AutoSize = True
        Me.lblAdress.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblAdress.Location = New System.Drawing.Point(84, 135)
        Me.lblAdress.Name = "lblAdress"
        Me.lblAdress.Size = New System.Drawing.Size(50, 18)
        Me.lblAdress.TabIndex = 0
        Me.lblAdress.Text = "Адрес"
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblPhone.Location = New System.Drawing.Point(73, 164)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(72, 18)
        Me.lblPhone.TabIndex = 0
        Me.lblPhone.Text = "Телефон"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.txtName.Location = New System.Drawing.Point(222, 73)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(329, 24)
        Me.txtName.TabIndex = 1
        '
        'txtSurname
        '
        Me.txtSurname.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.txtSurname.Location = New System.Drawing.Point(222, 102)
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.Size = New System.Drawing.Size(329, 24)
        Me.txtSurname.TabIndex = 2
        '
        'txtAdress
        '
        Me.txtAdress.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.txtAdress.Location = New System.Drawing.Point(222, 131)
        Me.txtAdress.Name = "txtAdress"
        Me.txtAdress.Size = New System.Drawing.Size(329, 24)
        Me.txtAdress.TabIndex = 3
        '
        'txtPhone
        '
        Me.txtPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.txtPhone.Location = New System.Drawing.Point(222, 160)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(329, 24)
        Me.txtPhone.TabIndex = 4
        '
        'lblCarModel
        '
        Me.lblCarModel.AutoSize = True
        Me.lblCarModel.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblCarModel.Location = New System.Drawing.Point(26, 193)
        Me.lblCarModel.Name = "lblCarModel"
        Me.lblCarModel.Size = New System.Drawing.Size(166, 18)
        Me.lblCarModel.TabIndex = 6
        Me.lblCarModel.Text = "Модел на автомобила"
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Toyota Avensis", "Mazda 6", "Renault Clio", "Toyota Land Cruser", "VW Mulovan", "Audi A4"})
        Me.ComboBox1.Location = New System.Drawing.Point(222, 189)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(329, 26)
        Me.ComboBox1.TabIndex = 7
        '
        'lblBonus
        '
        Me.lblBonus.AutoSize = True
        Me.lblBonus.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblBonus.Location = New System.Drawing.Point(10, 228)
        Me.lblBonus.Name = "lblBonus"
        Me.lblBonus.Size = New System.Drawing.Size(199, 18)
        Me.lblBonus.TabIndex = 8
        Me.lblBonus.Text = "Допълнително оборудване"
        '
        'chkChair
        '
        Me.chkChair.AutoSize = True
        Me.chkChair.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.chkChair.Location = New System.Drawing.Point(215, 226)
        Me.chkChair.Name = "chkChair"
        Me.chkChair.Size = New System.Drawing.Size(132, 22)
        Me.chkChair.TabIndex = 9
        Me.chkChair.Text = "Детско столче"
        Me.chkChair.UseVisualStyleBackColor = True
        '
        'chkSki
        '
        Me.chkSki.AutoSize = True
        Me.chkSki.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.chkSki.Location = New System.Drawing.Point(480, 226)
        Me.chkSki.Name = "chkSki"
        Me.chkSki.Size = New System.Drawing.Size(142, 22)
        Me.chkSki.TabIndex = 10
        Me.chkSki.Text = "Багажник за ски"
        Me.chkSki.UseVisualStyleBackColor = True
        '
        'chkChain
        '
        Me.chkChain.AutoSize = True
        Me.chkChain.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.chkChain.Location = New System.Drawing.Point(345, 226)
        Me.chkChain.Name = "chkChain"
        Me.chkChain.Size = New System.Drawing.Size(129, 22)
        Me.chkChain.TabIndex = 11
        Me.chkChain.Text = "Вериги за сняг"
        Me.chkChain.UseVisualStyleBackColor = True
        '
        'lblPayment
        '
        Me.lblPayment.AutoSize = True
        Me.lblPayment.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblPayment.Location = New System.Drawing.Point(42, 268)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(135, 18)
        Me.lblPayment.TabIndex = 12
        Me.lblPayment.Text = "Начин на плащане"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblDate.Location = New System.Drawing.Point(46, 306)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(126, 18)
        Me.lblDate.TabIndex = 16
        Me.lblDate.Text = "Дата на наемане"
        '
        'dtpStart
        '
        Me.dtpStart.Location = New System.Drawing.Point(288, 303)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(200, 20)
        Me.dtpStart.TabIndex = 17
        '
        'gbPayment
        '
        Me.gbPayment.Controls.Add(Me.optCash)
        Me.gbPayment.Controls.Add(Me.optBank)
        Me.gbPayment.Controls.Add(Me.optCredit)
        Me.gbPayment.Location = New System.Drawing.Point(221, 258)
        Me.gbPayment.Name = "gbPayment"
        Me.gbPayment.Size = New System.Drawing.Size(329, 35)
        Me.gbPayment.TabIndex = 18
        Me.gbPayment.TabStop = False
        Me.gbPayment.Text = "Плащане"
        '
        'optCredit
        '
        Me.optCredit.AutoSize = True
        Me.optCredit.Location = New System.Drawing.Point(6, 12)
        Me.optCredit.Name = "optCredit"
        Me.optCredit.Size = New System.Drawing.Size(105, 17)
        Me.optCredit.TabIndex = 0
        Me.optCredit.TabStop = True
        Me.optCredit.Text = "Кредитна карта"
        Me.optCredit.UseVisualStyleBackColor = True
        '
        'optBank
        '
        Me.optBank.AutoSize = True
        Me.optBank.Location = New System.Drawing.Point(136, 12)
        Me.optBank.Name = "optBank"
        Me.optBank.Size = New System.Drawing.Size(99, 17)
        Me.optBank.TabIndex = 1
        Me.optBank.TabStop = True
        Me.optBank.Text = "По банков път"
        Me.optBank.UseVisualStyleBackColor = True
        '
        'optCash
        '
        Me.optCash.AutoSize = True
        Me.optCash.Location = New System.Drawing.Point(260, 12)
        Me.optCash.Name = "optCash"
        Me.optCash.Size = New System.Drawing.Size(59, 17)
        Me.optCash.TabIndex = 2
        Me.optCash.TabStop = True
        Me.optCash.Text = "В брой"
        Me.optCash.UseVisualStyleBackColor = True
        '
        'lblDateReturn
        '
        Me.lblDateReturn.AutoSize = True
        Me.lblDateReturn.Font = New System.Drawing.Font("Stencil", 11.0!)
        Me.lblDateReturn.Location = New System.Drawing.Point(46, 345)
        Me.lblDateReturn.Name = "lblDateReturn"
        Me.lblDateReturn.Size = New System.Drawing.Size(127, 18)
        Me.lblDateReturn.TabIndex = 19
        Me.lblDateReturn.Text = "Дата на връщане"
        '
        'dtpEnd
        '
        Me.dtpEnd.Location = New System.Drawing.Point(288, 342)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(197, 20)
        Me.dtpEnd.TabIndex = 20
        '
        'formCarRental
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.ClientSize = New System.Drawing.Size(624, 441)
        Me.Controls.Add(Me.dtpEnd)
        Me.Controls.Add(Me.lblDateReturn)
        Me.Controls.Add(Me.gbPayment)
        Me.Controls.Add(Me.dtpStart)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.lblPayment)
        Me.Controls.Add(Me.chkChain)
        Me.Controls.Add(Me.chkSki)
        Me.Controls.Add(Me.chkChair)
        Me.Controls.Add(Me.lblBonus)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.lblCarModel)
        Me.Controls.Add(Me.txtPhone)
        Me.Controls.Add(Me.txtAdress)
        Me.Controls.Add(Me.txtSurname)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.lblPhone)
        Me.Controls.Add(Me.lblAdress)
        Me.Controls.Add(Me.lblSurname)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblFormHeading)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "formCarRental"
        Me.Text = "Автомобили под наем"
        Me.gbPayment.ResumeLayout(False)
        Me.gbPayment.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblFormHeading As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblSurname As System.Windows.Forms.Label
    Friend WithEvents lblAdress As System.Windows.Forms.Label
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtSurname As System.Windows.Forms.TextBox
    Friend WithEvents txtAdress As System.Windows.Forms.TextBox
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblCarModel As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblBonus As System.Windows.Forms.Label
    Friend WithEvents chkChair As System.Windows.Forms.CheckBox
    Friend WithEvents chkSki As System.Windows.Forms.CheckBox
    Friend WithEvents chkChain As System.Windows.Forms.CheckBox
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbPayment As System.Windows.Forms.GroupBox
    Friend WithEvents optCash As System.Windows.Forms.RadioButton
    Friend WithEvents optBank As System.Windows.Forms.RadioButton
    Friend WithEvents optCredit As System.Windows.Forms.RadioButton
    Friend WithEvents lblDateReturn As System.Windows.Forms.Label
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker

End Class
