﻿Public Class Form1
    Dim Weight As Double
    Dim Height As Double

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Weight = Val(txtWeight.Text)
        Height = Val(txtHeight.Text)

        lblBMI.Text = Format(Weight / (Height ^ 2), "##")

        If lblBMI.Text < 19 Then
            MsgBox("Вашето тегло е под нормата!", 0, "Внимание!")            
        End If
        If lblBMI.Text >= 19 Then
            If lblBMI.Text < 25 Then
                MsgBox("Вашето тегло е идеално!", 0, "Внимание!")
            End If
            If lblBMI.Text >= 25 And lblBMI.Text < 30 Then
                MsgBox("Вие имате наднормено тегло!", 0, "Внимание!")
            End If
            If lblBMI.Text >= 30 Then
                MsgBox("Вие имате затлъстяване!", 0, "Внимание!")
            End If
        End If
    End Sub
End Class
