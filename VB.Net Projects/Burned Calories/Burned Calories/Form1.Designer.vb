﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.lblSport = New System.Windows.Forms.Label()
        Me.txtTime = New System.Windows.Forms.TextBox()
        Me.cbSport = New System.Windows.Forms.ComboBox()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblHeader
        '
        Me.lblHeader.AutoSize = True
        Me.lblHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblHeader.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblHeader.Location = New System.Drawing.Point(5, 9)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(275, 50)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Изчисляване на изгорени " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "калории при спорт"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.lblTime.Location = New System.Drawing.Point(12, 98)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(118, 18)
        Me.lblTime.TabIndex = 1
        Me.lblTime.Text = "Време в минути"
        '
        'lblSport
        '
        Me.lblSport.AutoSize = True
        Me.lblSport.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.lblSport.Location = New System.Drawing.Point(185, 98)
        Me.lblSport.Name = "lblSport"
        Me.lblSport.Size = New System.Drawing.Size(51, 18)
        Me.lblSport.TabIndex = 2
        Me.lblSport.Text = "Спорт"
        '
        'txtTime
        '
        Me.txtTime.Location = New System.Drawing.Point(29, 131)
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(85, 20)
        Me.txtTime.TabIndex = 3
        '
        'cbSport
        '
        Me.cbSport.FormattingEnabled = True
        Me.cbSport.Items.AddRange(New Object() {"Аеробика", "Боулинг", "Волейбол", "Тенис на корт", "Футбол"})
        Me.cbSport.Location = New System.Drawing.Point(161, 130)
        Me.cbSport.Name = "cbSport"
        Me.cbSport.Size = New System.Drawing.Size(99, 21)
        Me.cbSport.TabIndex = 4
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(100, 180)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(85, 32)
        Me.btnCalculate.TabIndex = 5
        Me.btnCalculate.Text = "Изчисли"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 229)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.cbSport)
        Me.Controls.Add(Me.txtTime)
        Me.Controls.Add(Me.lblSport)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.lblHeader)
        Me.Name = "Form1"
        Me.Text = "Калкулатор за калории"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblSport As System.Windows.Forms.Label
    Friend WithEvents txtTime As System.Windows.Forms.TextBox
    Friend WithEvents cbSport As System.Windows.Forms.ComboBox
    Friend WithEvents btnCalculate As System.Windows.Forms.Button

End Class
