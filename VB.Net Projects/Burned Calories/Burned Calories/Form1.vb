﻿Public Class Form1
    Dim K As Double
    Private Sub txtTime_TextChanged(sender As Object, e As EventArgs) Handles txtTime.TextChanged
        K = Val(txtTime.Text) / 60
    End Sub
    Private Sub cbSport_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSport.SelectedIndexChanged


        Select Case cbSport.Text
            Case "Аеробика"
                K *= 500
            Case "Боулинг"
                K *= 200
            Case "Волейбол"
                K *= 220
            Case "Тенис на корт"
                K *= 540
            Case "Футбол"
                K *= 420
        End Select

    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        MsgBox("Чрез " & cbSport.Text & " Вие ще изгорите " & K & " калории за " & txtTime.Text & " минути", 0, "Внимание!")

        K = 0
        cbSport.Text = vbNullString
        txtTime.Text = vbNullString
    End Sub
End Class
