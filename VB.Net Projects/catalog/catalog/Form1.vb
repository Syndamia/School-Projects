﻿Imports System.ComponentModel

Public Class Form1
    ReadOnly Products As BindingList(Of Product) = New BindingList(Of Product) From {
        New Product("Магнитни наколенки", 59.9),
        New Product("Магнитен колан", 39.9),
        New Product("Магни Иър", 39.9),
        New Product("Масажираща седалка", 59.9),
        New Product("Сауна за лице", 49.9),
        New Product("Електрическо одеяло", 56.9),
        New Product("Инфраред масажор за тяло", 147.0),
        New Product("Гривна за баланс", 54.9),
        New Product("Цитрус джусер", 49.9),
        New Product("Апарат за кръвно налягане", 109.8)
    }
    ReadOnly ChosenProducts As BindingList(Of Product) = New BindingList(Of Product)

    Private Sub InitDataSources() Handles MyBase.Load
        cbProducts.DataSource = Products
        lbChosenProducts.DataSource = ChosenProducts
    End Sub

    Private Sub AddChosenProduct() Handles btnAddProduct.Click
        ChosenProducts.Add(cbProducts.SelectedItem)
    End Sub

    Private Sub RemoveChosenProduct() Handles btnRemoveProduct.Click
        ChosenProducts.Remove(lbChosenProducts.SelectedItem)
    End Sub

    Private Sub UpdateProductPriceLabel() Handles cbProducts.SelectedIndexChanged, MyBase.Load
        lblProductPrice.Text = FormatCurrency(cbProducts.SelectedItem.Price, 2)
    End Sub

    Private Sub UpdateSumLabel() Handles btnAddProduct.Click, btnRemoveProduct.Click, MyBase.Load
        lblSum.Text = FormatCurrency(ChosenProducts.Sum(Function(p) p.Price), 2)
    End Sub
End Class
