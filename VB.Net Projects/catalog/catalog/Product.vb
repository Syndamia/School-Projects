﻿Public Class Product
    Public Name As String
    Public Price As Single

    Public Sub New(name As String, price As Single)
        Me.Name = name
        Me.Price = price
    End Sub

    Public Overrides Function ToString() As String
        Return Me.Name
    End Function
End Class