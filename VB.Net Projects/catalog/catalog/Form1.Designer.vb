﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cbProducts = New System.Windows.Forms.ComboBox()
        Me.lblProducts = New System.Windows.Forms.Label()
        Me.lblProductPriceLabel = New System.Windows.Forms.Label()
        Me.lblProductPrice = New System.Windows.Forms.Label()
        Me.btnAddProduct = New System.Windows.Forms.Button()
        Me.btnRemoveProduct = New System.Windows.Forms.Button()
        Me.lbChosenProducts = New System.Windows.Forms.ListBox()
        Me.lblSumLabel = New System.Windows.Forms.Label()
        Me.lblSum = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cbProducts
        '
        Me.cbProducts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbProducts.FormattingEnabled = True
        Me.cbProducts.Location = New System.Drawing.Point(17, 53)
        Me.cbProducts.Name = "cbProducts"
        Me.cbProducts.Size = New System.Drawing.Size(289, 23)
        Me.cbProducts.TabIndex = 0
        '
        'lblProducts
        '
        Me.lblProducts.AutoSize = True
        Me.lblProducts.Location = New System.Drawing.Point(17, 22)
        Me.lblProducts.Name = "lblProducts"
        Me.lblProducts.Size = New System.Drawing.Size(98, 15)
        Me.lblProducts.TabIndex = 1
        Me.lblProducts.Text = "Избери продукт:"
        '
        'lblProductPriceLabel
        '
        Me.lblProductPriceLabel.AutoSize = True
        Me.lblProductPriceLabel.Location = New System.Drawing.Point(28, 94)
        Me.lblProductPriceLabel.Name = "lblProductPriceLabel"
        Me.lblProductPriceLabel.Size = New System.Drawing.Size(107, 15)
        Me.lblProductPriceLabel.TabIndex = 2
        Me.lblProductPriceLabel.Text = "Цена на продукта:"
        '
        'lblProductPrice
        '
        Me.lblProductPrice.AutoSize = True
        Me.lblProductPrice.Location = New System.Drawing.Point(210, 94)
        Me.lblProductPrice.Name = "lblProductPrice"
        Me.lblProductPrice.Size = New System.Drawing.Size(27, 15)
        Me.lblProductPrice.TabIndex = 3
        Me.lblProductPrice.Text = "ERR"
        '
        'btnAddProduct
        '
        Me.btnAddProduct.Location = New System.Drawing.Point(17, 134)
        Me.btnAddProduct.Name = "btnAddProduct"
        Me.btnAddProduct.Size = New System.Drawing.Size(118, 39)
        Me.btnAddProduct.TabIndex = 4
        Me.btnAddProduct.Text = "Добави"
        Me.btnAddProduct.UseVisualStyleBackColor = True
        '
        'btnRemoveProduct
        '
        Me.btnRemoveProduct.Location = New System.Drawing.Point(17, 179)
        Me.btnRemoveProduct.Name = "btnRemoveProduct"
        Me.btnRemoveProduct.Size = New System.Drawing.Size(118, 36)
        Me.btnRemoveProduct.TabIndex = 5
        Me.btnRemoveProduct.Text = "Премахни"
        Me.btnRemoveProduct.UseVisualStyleBackColor = True
        '
        'lbChosenProducts
        '
        Me.lbChosenProducts.FormattingEnabled = True
        Me.lbChosenProducts.ItemHeight = 15
        Me.lbChosenProducts.Location = New System.Drawing.Point(141, 120)
        Me.lbChosenProducts.Name = "lbChosenProducts"
        Me.lbChosenProducts.Size = New System.Drawing.Size(165, 109)
        Me.lbChosenProducts.TabIndex = 6
        '
        'lblSumLabel
        '
        Me.lblSumLabel.AutoSize = True
        Me.lblSumLabel.Location = New System.Drawing.Point(94, 245)
        Me.lblSumLabel.Name = "lblSumLabel"
        Me.lblSumLabel.Size = New System.Drawing.Size(41, 15)
        Me.lblSumLabel.TabIndex = 7
        Me.lblSumLabel.Text = "СУМА"
        '
        'lblSum
        '
        Me.lblSum.AutoSize = True
        Me.lblSum.Location = New System.Drawing.Point(141, 245)
        Me.lblSum.Name = "lblSum"
        Me.lblSum.Size = New System.Drawing.Size(27, 15)
        Me.lblSum.TabIndex = 8
        Me.lblSum.Text = "ERR"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(324, 275)
        Me.Controls.Add(Me.lblSum)
        Me.Controls.Add(Me.lblSumLabel)
        Me.Controls.Add(Me.lbChosenProducts)
        Me.Controls.Add(Me.btnRemoveProduct)
        Me.Controls.Add(Me.btnAddProduct)
        Me.Controls.Add(Me.lblProductPrice)
        Me.Controls.Add(Me.lblProductPriceLabel)
        Me.Controls.Add(Me.lblProducts)
        Me.Controls.Add(Me.cbProducts)
        Me.Name = "Form1"
        Me.Text = "Пазаруване по каталог"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbProducts As ComboBox
    Friend WithEvents lblProducts As Label
    Friend WithEvents lblProductPriceLabel As Label
    Friend WithEvents lblProductPrice As Label
    Friend WithEvents btnAddProduct As Button
    Friend WithEvents btnRemoveProduct As Button
    Friend WithEvents lbChosenProducts As ListBox
    Friend WithEvents lblSumLabel As Label
    Friend WithEvents lblSum As Label
End Class