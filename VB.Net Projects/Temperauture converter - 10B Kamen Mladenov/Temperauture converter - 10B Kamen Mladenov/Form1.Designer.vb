﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.lblHeading = New System.Windows.Forms.Label()
        Me.pctTheromiter = New System.Windows.Forms.PictureBox()
        Me.lblC = New System.Windows.Forms.Label()
        Me.lblF = New System.Windows.Forms.Label()
        Me.txtC = New System.Windows.Forms.TextBox()
        Me.txtF = New System.Windows.Forms.TextBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnSolve = New System.Windows.Forms.Button()
        CType(Me.pctTheromiter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblHeading
        '
        Me.lblHeading.AutoSize = True
        Me.lblHeading.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblHeading.ForeColor = System.Drawing.Color.White
        Me.lblHeading.Location = New System.Drawing.Point(24, 9)
        Me.lblHeading.MinimumSize = New System.Drawing.Size(350, 25)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.Size = New System.Drawing.Size(350, 25)
        Me.lblHeading.TabIndex = 0
        Me.lblHeading.Text = "Целзии - Фаренхайт конвертор"
        Me.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pctTheromiter
        '
        Me.pctTheromiter.BackgroundImage = CType(resources.GetObject("pctTheromiter.BackgroundImage"), System.Drawing.Image)
        Me.pctTheromiter.Image = CType(resources.GetObject("pctTheromiter.Image"), System.Drawing.Image)
        Me.pctTheromiter.Location = New System.Drawing.Point(12, 37)
        Me.pctTheromiter.Name = "pctTheromiter"
        Me.pctTheromiter.Size = New System.Drawing.Size(123, 206)
        Me.pctTheromiter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pctTheromiter.TabIndex = 1
        Me.pctTheromiter.TabStop = False
        '
        'lblC
        '
        Me.lblC.AutoSize = True
        Me.lblC.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblC.ForeColor = System.Drawing.Color.White
        Me.lblC.Location = New System.Drawing.Point(197, 49)
        Me.lblC.Name = "lblC"
        Me.lblC.Size = New System.Drawing.Size(138, 16)
        Me.lblC.TabIndex = 2
        Me.lblC.Text = "Градуси по Целзии"
        '
        'lblF
        '
        Me.lblF.AutoSize = True
        Me.lblF.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblF.ForeColor = System.Drawing.Color.White
        Me.lblF.Location = New System.Drawing.Point(188, 125)
        Me.lblF.Name = "lblF"
        Me.lblF.Size = New System.Drawing.Size(162, 16)
        Me.lblF.TabIndex = 3
        Me.lblF.Text = "Градуси по Фаренхайт"
        '
        'txtC
        '
        Me.txtC.Location = New System.Drawing.Point(221, 83)
        Me.txtC.Name = "txtC"
        Me.txtC.Size = New System.Drawing.Size(86, 20)
        Me.txtC.TabIndex = 4
        '
        'txtF
        '
        Me.txtF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.txtF.Location = New System.Drawing.Point(221, 159)
        Me.txtF.Name = "txtF"
        Me.txtF.Size = New System.Drawing.Size(86, 20)
        Me.txtF.TabIndex = 5
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(277, 203)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(85, 31)
        Me.btnClear.TabIndex = 6
        Me.btnClear.Text = "Изчисти"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnSolve
        '
        Me.btnSolve.Location = New System.Drawing.Point(158, 203)
        Me.btnSolve.Name = "btnSolve"
        Me.btnSolve.Size = New System.Drawing.Size(85, 31)
        Me.btnSolve.TabIndex = 7
        Me.btnSolve.Text = "Изчисли"
        Me.btnSolve.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.SteelBlue
        Me.ClientSize = New System.Drawing.Size(404, 261)
        Me.Controls.Add(Me.btnSolve)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.txtF)
        Me.Controls.Add(Me.txtC)
        Me.Controls.Add(Me.lblF)
        Me.Controls.Add(Me.lblC)
        Me.Controls.Add(Me.pctTheromiter)
        Me.Controls.Add(Me.lblHeading)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.pctTheromiter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblHeading As System.Windows.Forms.Label
    Friend WithEvents pctTheromiter As System.Windows.Forms.PictureBox
    Friend WithEvents lblC As System.Windows.Forms.Label
    Friend WithEvents lblF As System.Windows.Forms.Label
    Friend WithEvents txtC As System.Windows.Forms.TextBox
    Friend WithEvents txtF As System.Windows.Forms.TextBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnSolve As System.Windows.Forms.Button

End Class
