﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.pboxMap = New System.Windows.Forms.PictureBox()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.lblPass = New System.Windows.Forms.Label()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.btnEnter = New System.Windows.Forms.Button()
        Me.lblWrongPass = New System.Windows.Forms.Label()
        CType(Me.pboxMap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pboxMap
        '
        Me.pboxMap.ErrorImage = CType(resources.GetObject("pboxMap.ErrorImage"), System.Drawing.Image)
        Me.pboxMap.Image = CType(resources.GetObject("pboxMap.Image"), System.Drawing.Image)
        Me.pboxMap.InitialImage = CType(resources.GetObject("pboxMap.InitialImage"), System.Drawing.Image)
        Me.pboxMap.Location = New System.Drawing.Point(3, 2)
        Me.pboxMap.Name = "pboxMap"
        Me.pboxMap.Size = New System.Drawing.Size(364, 247)
        Me.pboxMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pboxMap.TabIndex = 0
        Me.pboxMap.TabStop = False
        Me.pboxMap.Visible = False
        '
        'lblHeader
        '
        Me.lblHeader.AutoSize = True
        Me.lblHeader.Font = New System.Drawing.Font("Monotype Corsiva", 20.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblHeader.Location = New System.Drawing.Point(76, 38)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(226, 33)
        Me.lblHeader.TabIndex = 1
        Me.lblHeader.Text = "Карта на съкровища"
        '
        'lblPass
        '
        Me.lblPass.AutoSize = True
        Me.lblPass.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblPass.Location = New System.Drawing.Point(65, 89)
        Me.lblPass.Name = "lblPass"
        Me.lblPass.Size = New System.Drawing.Size(241, 24)
        Me.lblPass.TabIndex = 2
        Me.lblPass.Text = "Въведи парола за достъп"
        '
        'txtPass
        '
        Me.txtPass.Location = New System.Drawing.Point(142, 133)
        Me.txtPass.Name = "txtPass"
        Me.txtPass.Size = New System.Drawing.Size(86, 20)
        Me.txtPass.TabIndex = 3
        '
        'btnEnter
        '
        Me.btnEnter.ForeColor = System.Drawing.Color.Black
        Me.btnEnter.Location = New System.Drawing.Point(141, 174)
        Me.btnEnter.Name = "btnEnter"
        Me.btnEnter.Size = New System.Drawing.Size(89, 22)
        Me.btnEnter.TabIndex = 4
        Me.btnEnter.Text = "Влез"
        Me.btnEnter.UseVisualStyleBackColor = True
        '
        'lblWrongPass
        '
        Me.lblWrongPass.AutoSize = True
        Me.lblWrongPass.Enabled = False
        Me.lblWrongPass.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblWrongPass.ForeColor = System.Drawing.Color.DarkRed
        Me.lblWrongPass.Location = New System.Drawing.Point(89, 212)
        Me.lblWrongPass.Name = "lblWrongPass"
        Me.lblWrongPass.Size = New System.Drawing.Size(193, 25)
        Me.lblWrongPass.TabIndex = 5
        Me.lblWrongPass.Text = "ГРЕШНА ПАРОЛА"
        Me.lblWrongPass.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 261)
        Me.Controls.Add(Me.lblWrongPass)
        Me.Controls.Add(Me.btnEnter)
        Me.Controls.Add(Me.txtPass)
        Me.Controls.Add(Me.lblPass)
        Me.Controls.Add(Me.lblHeader)
        Me.Controls.Add(Me.pboxMap)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.pboxMap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pboxMap As System.Windows.Forms.PictureBox
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents lblPass As System.Windows.Forms.Label
    Friend WithEvents txtPass As System.Windows.Forms.TextBox
    Friend WithEvents btnEnter As System.Windows.Forms.Button
    Friend WithEvents lblWrongPass As System.Windows.Forms.Label

End Class
