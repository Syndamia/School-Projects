' 1 Задача: Програма с въведени C° се изчислява и въвежда F°

Public Class Form1
	Dim C as single
	Dim F as single

	Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
		C = txtC.Text
		F = C * 1.8 + 32
		txtF.Text = F
	End Sub

	Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
		txtF.Text = vbNullString
		txtC.Text = vbNullString
	End Sub

	Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
		Close()
	End Sub
End Class

' 2 Задача: Въведени F° в C°

Public Class Form1
	Dim F as single
	Dim C as single

	Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
		F = txtF.Text
		C = Val(F - 32) / 1.8
		txtC.Text = C
	End Sub

	Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
		txtF.Text = vbNullString
		txtC.Text = vbNullString
	End Sub

	Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
		Close()
	End Sub
End Class

' 3 Задача: Размери на баня в m, размери на врата в m, изчисляват се броя плочки за облицоване (a*b*c е обем, трябва лицето на стените и пода)

Public Class Form1
	Dim BW as single
	Dim BL as single
	Dim BH as single
	Dim DW as single
	Dim DH as single
	Dim T as single

	Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
		BW = txtBathW.Text
		BL = txtBathL.Text
		BH = txtBathH.Text
		DW = txtDoorW.Text
		DH = txtDoorH.Text
		T = BW*BL + 2*BW*BH + 2*BL*BH - DW*DH
		lblTiles.Text = T
	End Sub

	Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
		txtBathW = vbNullString
		txtBathL = vbNullString
		txtBathH = vbNullString
		txtDoorW = vbNullString
		txtDoorH = vbNullString
		lblTiles = vbNullString
	End Sub

	Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
		Close()
	End Sub
End Class
