﻿Public Class Form1
    Dim Coeff As Integer
    Dim Status As String
    Dim D1 As Date
    Dim D2 As Date
    Dim Days As Integer

    Private Sub rbPhysical_CheckedChanged(sender As Object, e As EventArgs) Handles rbPhysical.CheckedChanged
        Coeff = 23
        Status = "физическо"
    End Sub

    Private Sub rbEmotional_CheckedChanged(sender As Object, e As EventArgs) Handles rbEmotional.CheckedChanged
        Coeff = 28
        Status = "емоционално"
    End Sub

    Private Sub rbIntellectual_CheckedChanged(sender As Object, e As EventArgs) Handles rbIntellectual.CheckedChanged
        Coeff = 33
        Status = "интелектуално"
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        D1 = BirthDay.Text
        D2 = Today
        Days = DateDiff(DateInterval.Day, D1, D2)
        MsgBox("Вашият процент за " & Status & " състояние е " & Format(Math.Sin(360 * Days / Coeff), "00%"), 0, "Внимание!")
    End Sub
End Class
