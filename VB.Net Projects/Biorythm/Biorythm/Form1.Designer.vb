﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.lblBirthDay = New System.Windows.Forms.Label()
        Me.BirthDay = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbIntellectual = New System.Windows.Forms.RadioButton()
        Me.rbEmotional = New System.Windows.Forms.RadioButton()
        Me.rbPhysical = New System.Windows.Forms.RadioButton()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblHeader
        '
        Me.lblHeader.AutoSize = True
        Me.lblHeader.Location = New System.Drawing.Point(62, 18)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(133, 13)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Калкулатор на биоритми"
        '
        'lblBirthDay
        '
        Me.lblBirthDay.AutoSize = True
        Me.lblBirthDay.Location = New System.Drawing.Point(21, 68)
        Me.lblBirthDay.Name = "lblBirthDay"
        Me.lblBirthDay.Size = New System.Drawing.Size(95, 13)
        Me.lblBirthDay.TabIndex = 1
        Me.lblBirthDay.Text = "Дата на раждане"
        '
        'BirthDay
        '
        Me.BirthDay.Location = New System.Drawing.Point(163, 68)
        Me.BirthDay.Name = "BirthDay"
        Me.BirthDay.Size = New System.Drawing.Size(90, 20)
        Me.BirthDay.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbIntellectual)
        Me.GroupBox1.Controls.Add(Me.rbEmotional)
        Me.GroupBox1.Controls.Add(Me.rbPhysical)
        Me.GroupBox1.Location = New System.Drawing.Point(56, 94)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(154, 113)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'rbIntellectual
        '
        Me.rbIntellectual.AutoSize = True
        Me.rbIntellectual.Location = New System.Drawing.Point(6, 78)
        Me.rbIntellectual.Name = "rbIntellectual"
        Me.rbIntellectual.Size = New System.Drawing.Size(153, 17)
        Me.rbIntellectual.TabIndex = 2
        Me.rbIntellectual.TabStop = True
        Me.rbIntellectual.Text = "Интелигентно състояние"
        Me.rbIntellectual.UseVisualStyleBackColor = True
        '
        'rbEmotional
        '
        Me.rbEmotional.AutoSize = True
        Me.rbEmotional.Location = New System.Drawing.Point(6, 44)
        Me.rbEmotional.Name = "rbEmotional"
        Me.rbEmotional.Size = New System.Drawing.Size(151, 17)
        Me.rbEmotional.TabIndex = 1
        Me.rbEmotional.TabStop = True
        Me.rbEmotional.Text = "Емоционално състояние"
        Me.rbEmotional.UseVisualStyleBackColor = True
        '
        'rbPhysical
        '
        Me.rbPhysical.AutoSize = True
        Me.rbPhysical.Location = New System.Drawing.Point(6, 9)
        Me.rbPhysical.Name = "rbPhysical"
        Me.rbPhysical.Size = New System.Drawing.Size(140, 17)
        Me.rbPhysical.TabIndex = 0
        Me.rbPhysical.TabStop = True
        Me.rbPhysical.Text = "Физическо състояние"
        Me.rbPhysical.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(80, 213)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(121, 33)
        Me.btnCalculate.TabIndex = 4
        Me.btnCalculate.Text = "Изчисли"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.BirthDay)
        Me.Controls.Add(Me.lblBirthDay)
        Me.Controls.Add(Me.lblHeader)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents lblBirthDay As System.Windows.Forms.Label
    Friend WithEvents BirthDay As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbIntellectual As System.Windows.Forms.RadioButton
    Friend WithEvents rbEmotional As System.Windows.Forms.RadioButton
    Friend WithEvents rbPhysical As System.Windows.Forms.RadioButton
    Friend WithEvents btnCalculate As System.Windows.Forms.Button

End Class
