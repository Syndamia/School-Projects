﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.cbNumberNames = New System.Windows.Forms.ComboBox()
        Me.txtNumberGuess = New System.Windows.Forms.TextBox()
        Me.btnCheck = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Наименование на число:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Въведено число:"
        '
        'lblResult
        '
        Me.lblResult.AutoSize = True
        Me.lblResult.Location = New System.Drawing.Point(258, 114)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(27, 15)
        Me.lblResult.TabIndex = 2
        Me.lblResult.Text = "ERR"
        '
        'cbNumberNames
        '
        Me.cbNumberNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbNumberNames.FormattingEnabled = True
        Me.cbNumberNames.Location = New System.Drawing.Point(172, 29)
        Me.cbNumberNames.Name = "cbNumberNames"
        Me.cbNumberNames.Size = New System.Drawing.Size(213, 23)
        Me.cbNumberNames.TabIndex = 3
        '
        'txtNumberGuess
        '
        Me.txtNumberGuess.Location = New System.Drawing.Point(172, 61)
        Me.txtNumberGuess.Name = "txtNumberGuess"
        Me.txtNumberGuess.Size = New System.Drawing.Size(213, 23)
        Me.txtNumberGuess.TabIndex = 4
        '
        'btnCheck
        '
        Me.btnCheck.Location = New System.Drawing.Point(12, 110)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(146, 23)
        Me.btnCheck.TabIndex = 5
        Me.btnCheck.Text = "Провери"
        Me.btnCheck.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(401, 160)
        Me.Controls.Add(Me.btnCheck)
        Me.Controls.Add(Me.txtNumberGuess)
        Me.Controls.Add(Me.cbNumberNames)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.Text = "English Numbers"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblResult As Label
    Friend WithEvents cbNumberNames As ComboBox
    Friend WithEvents txtNumberGuess As TextBox
    Friend WithEvents btnCheck As Button
End Class
