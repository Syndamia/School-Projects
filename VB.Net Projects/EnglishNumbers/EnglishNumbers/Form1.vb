﻿Imports System.ComponentModel

Public Class Form1
    Private ReadOnly numberNames = New BindingList(Of String) From {
        "Zero",
        "One",
        "Two",
        "Three",
        "Four",
        "Five",
        "Six",
        "Seven",
        "Eight",
        "Nine",
        "Ten"
    }

    Private Sub Init() Handles Me.Load
        cbNumberNames.DataSource = numberNames
        lblResult.Text = String.Empty
    End Sub

    Private Sub CheckGuess() Handles btnCheck.Click
        ' Val makes the String a number by removing all non-number characters,
        ' but if there are no numbers, it returns 0, which mustn't happen
        If cbNumberNames.SelectedIndex = Val(txtNumberGuess.Text) And txtNumberGuess.Text.Any(Function(x) Char.IsDigit(x)) Then
            lblResult.Text = "Вярно!"
            lblResult.ForeColor = Color.Green
        Else
            lblResult.Text = "Грешно!"
            lblResult.ForeColor = Color.Red
        End If
    End Sub
End Class